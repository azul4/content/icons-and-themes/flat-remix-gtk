# flat-remix-gtk

Flat Remix GTK theme is a pretty simple gtk window theme inspired on material design following a modern design using "flat" colors with high contrasts and sharp borders.

https://drasite.com/flat-remix-gtk

https://github.com/daniruiz/Flat-Remix-GTK

<br><br>
How to clone this repository:

```
git clone https://gitlab.com/azul4/content/icons-and-themes/flat-remix-gtk.git
```

